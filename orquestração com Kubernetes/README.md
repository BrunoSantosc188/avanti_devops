# Módulo 1: Introdução ao DevOps
Unidade 1: Introdução ao desenvolvimento moderno de software
Unidade 2: Definição de DevOps
Unidade 3: Benefícios do DevOps
Unidade 4:. Princípios e cultura DevOps
Unidade 5: Papéis e responsabilidades no DevOps

# Módulo 2: Fundamentos de Linux e Infraestrutura como código
Unidade 1: Introdução ao sistema operacional Linux
Unidade 2: Utilizando Linux em máquinas Virtuais
Unidade 3: Criando MV Linux utilizando o Vagrant
Unidade 4: Comandos básicos do Linux
Unidade 5: Gerenciamento de pacotes
Unidade 6: Gestão de usuários e permissões
Unidade 7: Automação de tarefas com scripts em shell
Unidade 8: Instalação e configuração de um servidor WEB
Unidade 9: Projeto prático de automação e provisionamento de um servidor Linux

# Módulo 3: Contêineres com Docker
Unidade 1: Introdução aos contêineres e virtualização
Unidade 2:  Conceitos básicos do Docker
Unidade 3: Instalação e configuração do Docker
Unidade 4: Construção de imagens de contêiner
Unidade 5: Execução e gerenciamento de contêineres
Unidade 6: Rede e armazenamento no Docker
Unidade 7: Projeto prático de publicação de uma aplicação conteinerizada

# Módulo 4: Orquestração com Kubernetes
Unidade 1: Introdução ao Kubernetes
Unidade 2: Arquitetura do Kubernetes
Unidade 3: Configuração e instalação do Kubernetes utilizando MiniKube
Unidade 4: Implantação e gerenciamento de aplicativos no Kubernetes
Unidade 5: Utilizando um cluster Kubernetes em Nuvem
Unidade 6: Escalonamento e balanceamento de carga
Unidade 7: Monitoramento e resiliência no Kubernetes
Unidade 8: Projeto prático de publicação de uma aplicação em um cluster Kubernetes

# Módulo 5: CI/CD (Continuous Integration/Continuous Deployment) com GitLab
Unidade 1: Introdução ao CI/CD
Unidade 2: Práticas de integração contínua
Unidade 3: Entendendo um pipeline
Unidade 4: Conceitos e arquitetura do GitLab
Unidade 5: Pipeline: do Desenvolvimento a produção
Unidade 6: Implementação de um fluxo de CI/CD utilizando Docker, Kubernetes e ferramentas de CI/CD
